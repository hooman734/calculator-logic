package Calculator;
import java.util.Stack;

public class State {
private double result;
private Operation current;
private final Stack<Operation> history;

public State() {
    result = 0.0;
    current = new Operation();
    history = new Stack<>();
}

private void pushToHistory() {
    history.push(current);
    current = new Operation();
}

public void recede() {
    if (!history.isEmpty()) {
        current = history.pop();
    }
}

public double getResult() {
    return result;
}

@Override
public String toString() {
    return Double.toString(current.firstOperand)
      .concat(current.operator
        .concat(Double.toString(current.secondOperand)
          .concat("=")
          .concat(Double.toString(result))));
}

public void setOperation(double input1, String op, double input2) {
    pushToHistory();
    current.firstOperand = input1;
    current.operator = op.trim();
    current.secondOperand = input2;
}

public Operation getState() {
    return current;
}

public void setResult(double result) {
    this.result = result;
}

}

