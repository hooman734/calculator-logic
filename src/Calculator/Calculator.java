package Calculator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

public class Calculator {

    public static List<String> calculateHelper (char op, String segment) {
        String operator = "(?<=\\d)\\s*["+ op + "]";
        ArrayList<String> temp = new ArrayList<>(Arrays.asList(Pattern.compile(operator, Pattern.CASE_INSENSITIVE).split(segment)));
        ArrayList<String> result = new ArrayList<>();
        for (String element: temp) {
            result.add(Pattern.compile("\\s*").matcher(element).replaceAll("").trim());
        }
        return result;
    }

    public static double evaluateDivision (String segment) {
        List<String> operands = calculateHelper('/', segment);
        double result = 1.0;
        try {
            result = Double.parseDouble(operands.remove(0));
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }

        for (String operand : operands) {
            try {
                result /= Double.parseDouble(operand);
            } catch (IllegalArgumentException e) {
                System.out.println(e.getMessage());
            }
        }
        return result;
    }

    public static double evaluator(char op, String segment) {
        double result = 0.0;
        switch (op) {
            case '+':
                List<String> shouldBeAdded = calculateHelper('+', segment);
                result = 0.0;
                for (String seg1 : shouldBeAdded) {
                    result += evaluator('-', seg1);
                }
                break;

            case '-':
                List<String> shouldBeReduced = calculateHelper('-', segment);
                result = evaluator('*', shouldBeReduced.remove(0));
                for (String seg2 : shouldBeReduced) {
                    result -= evaluator('*', seg2);
                }
                break;

            case '*':
                List<String> shouldBeMultiplied = calculateHelper('*', segment);
                result = 1.0;
                for (String seg3 : shouldBeMultiplied) {
                    result *= evaluator('/', seg3);
                }
                break;

            case '/':
                result = evaluateDivision(segment);
                break;

        }
        return result;
    }
}



